import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FlexLayoutModule} from '@angular/flex-layout';
import { FormsModule} from '@angular/forms';
import { MainAppComponent } from './mainApp.component';
import { appRoutes} from './routes';
import { NavBarComponent} from './navigation/navbar.component';
import { HomePageComponent } from './pages/home/HomePage.component';
import { CurrentWorkComponent } from './pages/currentWork/currentWork.component';
import { ContactMeComponent } from './pages/contactPage/contactMe.component';
import { InProgressComponent } from './pages/inProgress/inProgress.component';
import { WorkOneComponent } from './pages/currentWork/works/workOne.component';
import { AboutMeComponent } from './pages/About/aboutMe.component';
import { ModalWindowComponent } from './common/modalWindow.component';
import { CarouselComponent } from './common/carousel.component';
import { JQ_TOKEN } from './services/jQuery.service';
import { MessageService } from './services/message.service';
import { CurrentWorkData } from './services/currentWorkData.service';
import { AboutMeData } from './services/aboutMeData.service';
import { AboutMePopoverData } from './services/aboutMePopoverData.service';
import { InProgress } from './services/inProgressData.service';
import { PopoverModule } from 'ng2-popover';
import { PopoverContent } from './common/popoverContent.component';
import { ThumbnailData } from './services/thumbnailData.service';
import { Test } from './pages/testFoundation/ftest';
import { BackgroundImage } from './common/backgroundImage.component';
const jQuery = window['$'];

@NgModule({
  declarations: [
    NavBarComponent,
    HomePageComponent,
    MainAppComponent,
    CurrentWorkComponent,
    ContactMeComponent,
    InProgressComponent,
    WorkOneComponent,
    AboutMeComponent,
    ModalWindowComponent,
    CarouselComponent,
    PopoverContent,
    Test
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    PopoverModule
  ],
  providers: [
    CurrentWorkData,
    InProgress,
    ThumbnailData,
    AboutMePopoverData,
    AboutMeData,
    MessageService,
    { provide: JQ_TOKEN, useValue: jQuery },
//    SetBackground,
    BackgroundImage
  ],
  bootstrap: [MainAppComponent]
})
export class AppModule { }
