import { Injectable, Inject } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, 
    RouterStateSnapshot } from '@angular/router';
import { JQ_TOKEN } from '../services/jQuery.service';
declare var $: any;

@Injectable()
export class SetBackground implements CanActivate {
    backgound = 'assets/background3.jpg';
    constructor(@Inject(JQ_TOKEN) private $: any, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        this.$('#background').css('background-image', 'url(' + this.backgound + ')');
        return true;
    }
}