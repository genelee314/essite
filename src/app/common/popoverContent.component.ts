import { Component, OnInit, Input } from '@angular/core';
import { IThumbnailData } from '../data/IThumbnailData';
declare var $: any;

@Component({
    selector: 'popoverContent',
    templateUrl: 'popoverContent.component.html'
})

export class PopoverContent implements OnInit {
    @Input() thumbNail: IThumbnailData;
    @Input() poButton: string;
    @Input() poPicture: string;
    @Input() poIndex: number;
    hidePopover = true;
    thumbnails: string;
    description: string;
    picture: string;
    borderColor: string;
    buttonClass: string;
    color: string;
   
    ngOnInit() {
        $('img#thumbnail:eq(' + (this.poIndex) + ')').addClass('mouseOut');
        this.hidePopover = true;
        this.thumbnails = this.thumbNail.thumbnail;
        this.description = this.thumbNail.description;
        this.picture = this.thumbNail.picture;
        this.buttonClass = this.poButton;
   }
   
    showPopover1(state: boolean) {
        this.hidePopover = state;
   }
   
   mouseOver(){
    this.hidePopover = false;
    //   $('img#thumbnail:eq(' + (this.poIndex) + ')').removeClass('mouseOut');
    //    $('img#thumbnail:eq( '+ (this.poIndex) + ')').addClass('mouseOver');
        this.color = $('img#thumbnail:eq(' + (this.poIndex) + ')').css('border-color');
        $('img#thumbnail:eq( '+ (this.poIndex) + ')').css('border-color', 'gold');
    }

   mouseOut() {
    //$('img#thumbnail:eq(' + (this.poIndex) + ')').removeClass('mouseOver');
   // $('img#thumbnail:eq(' + (this.poIndex) + ')').addClass('mouseOut');
      $('img#thumbnail:eq( '+ (this.poIndex) + ')').css('border-color', this.color);
  
    }

    linkOver(){
        $('.link').removeClass('linkOut');
        $('.link').addClass('linkOver');
    }
    
    linkOut() {
        $('.link').removeClass('linkOver');
        $('.link').addClass('linkOut');
    }
}
