export interface IAboutMeData {
    pictureUrl: string;
    text: string;
}
