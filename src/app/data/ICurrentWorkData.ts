import {IThumbnail} from './IThumbnailData';

export interface ICurrentWorkData {
    projectNumber: number;
    backGroundUrl: string;
    pictureUrls: string [];
    pictureThumbnail: IThumbnail ;
    title: string;
    description: string;
    text: any;
}
