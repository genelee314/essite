export interface IInProgress {
    postNumber: number;
    title: string;
    description: string;
    pictureUrls: string [];
}
