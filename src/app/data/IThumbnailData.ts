export interface IThumbnail {
    thumbnail: string;
    picture: string;
    description: string;
}