import { Component, OnInit, Inject} from '@angular/core';
import {CurrentWorkData } from '../../../services/currentWorkData.service';
import {ICurrentWorkData } from '../../../data/ICurrentWorkData';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { BackgroundImage } from '../../../common/backgroundImage.component';

@Component({
   templateUrl: 'workOne.component.1.html',   
})

export class WorkOneComponent implements OnInit {
    private currentWork: any ;
    private videoImage = '../../assets/workProgress/VID_20181028_150336.mp4';
  //  private videoImage = 'https://www.youtube.com/watch?v=toKVC7cNqmQ';
    private hidePopover = true;
    private workData: ICurrentWorkData[];
    private description: any;
    private router: Router;
    private title: string;
    private index: number;
    private workPicture: string;
    private workButton: string;
    private background = '../../assets/backgroundColor.jpg';
    //private image = 'assets/S2_Adjusted_6.png';
    private image = 'assets/workProgress/SAM_763_T.png';

    constructor( private currentWorkData: CurrentWorkData, private backgroundImage: BackgroundImage,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
         this.currentWork = this.currentWorkData.getCurrentSingleWork(this.route.snapshot.params['id']);
         this.description = this.currentWork.text;
         this.title = this.currentWork.title;
         this.workData = this.currentWorkData.getCurrentWorkData();
         this.hidePopover = true;
         this.workPicture = 'workPicture';
         this.workButton = 'workButton mouseOut';
         this.backgroundImage.setImage(this.background);
    }

    displayPic(index: number) {
        this.hidePopover = false;
    }

    showPopover0(index: number) {
        this.hidePopover = false;
    }
}




