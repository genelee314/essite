import { Component, OnDestroy, OnInit, Inject, OnChanges,  } from '@angular/core';
import { CurrentWorkData } from '../../services/currentWorkData.service';
//import { ICurrentWorkData } from '../../data/ICurrentWorkData';
import { BackgroundImage } from '../../common/backgroundImage.component';
import { rootRoute } from '@angular/router/src/router_module';
import { element } from 'protractor';
declare var $: any;

@Component({
    selector: 'app-home-page',
    templateUrl: 'homePage.component.5.html'
})

export class HomePageComponent implements OnInit {
    private picture1 = '/assets/SAM_2000.jpg';
    private picture2 = '/assets/SAM_0763.JPG';
    private picture3 = '/assets/SAM_0763.JPG';
    //private picture3 = '/assets/SAM_763_T.png';
    private geneImage = '/assets/SAM_0763.JPG';
    private background = '/assets/backgroundColor.jpg';
    private index = 0;
    private timerToken: any;
    private homePage1 = 'assets/S2_Adjusted_6.png';
    private homePage2 = 'assets/SAM_770_Trans1.png';
    private homePage3 = '/assets/SAM_763_T.png';
 
    constructor(private currentWorkData: CurrentWorkData, private backgroundImage: BackgroundImage ) {
    }

    ngOnInit() {
      //  this.currentWork = this.currentWorkData.getCurrentSingleWork(0);
      
      this.backgroundImage.setImage(this.background);
      this.slideShow();
      $('.fadein img').hide();
    }

    slideShow() {
      //  $('.fadein img').hide();
        $('.fadein img').hide().fadeIn(3000).fadeOut(1750);
        this.timerToken = setInterval(function () {
            $('.fadein :first-child').hide()
                .next('img').fadeIn(1750).fadeOut(1750)
                .end().appendTo('.fadein');
        }, 4500);
    } 

    mouseOver(){
        
        $('.funLink')[0].setAttribute('color', 'green');
    //        $('img#thumbnail:eq( '+ (this.poIndex) + ')').addClass('mouseOver');
        }
    
       mouseOut() {
        $('img#thumbnail:eq(' + (this.poIndex) + ')').removeClass('mouseOver');
        $('img#thumbnail:eq(' + (this.poIndex) + ')').addClass('mouseOut');
        }

    ngOnDestroy() {
        if (this.timerToken) {
            clearInterval(this.timerToken);
        }
    }
}
