import { Component,  OnInit } from '@angular/core';
import { IInProgress } from '../../data/IInProgressData';
import {InProgress } from '../../services/inProgressData.service';
import { MessageService } from '../../services/message.service';

@Component({
    selector: 'app-progress-page',
    templateUrl: 'inProgress.component.html'
})

export class InProgressComponent implements OnInit{
    inProgress: IInProgress[];
        
    constructor(private messageService: MessageService, private inProgressData: InProgress) {
        this.messageService.sendMessage('base');
    }

    ngOnInit() {
        
        this.length = this.inProgressData.getLength();
        this.inProgress = this.inProgressData.getInProgressData_();
    }
}