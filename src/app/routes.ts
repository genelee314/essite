import { Routes, RouterModule } from '@angular/router';
import { CurrentWorkComponent } from './pages/currentWork/currentWork.component';
import { InProgressComponent } from './pages/inProgress/inProgress.component';
import { HomePageComponent } from './pages/home/HomePage.component';
import { AboutMeComponent } from './pages/About/aboutMe.component';
import { ContactMeComponent } from './pages/contactPage/contactMe.component';
// import { Error404Component } from './errors/404.component';
import { WorkOneComponent } from './pages/currentWork/works/workOne.component';
import { Test } from './pages/testFoundation/ftest';

export const appRoutes: Routes = [
 /*   { path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent']},
    { path: '404', component: Error404Component},
    { path: 'events', component: EventsListComponent, resolve: { events: EventListResolver }},
    { path: 'events/:id', component: EventDetailsComponent, canActivate: [EventRouteActivator] },
    { path: 'events/session/new', component: CreateSessionComponent},
    { path: '', redirectTo: '/events', pathMatch: 'full' },
  //  { path: 'user', loadChildren: 'app/user/user.module#UserModule' }
    { path: 'user/login', component: LoginComponent}, */
   // { path: '', redirectTo: 'home', pathMatch: 'full' },
   // { path: 'home', component: HomePageComponent, canActivate: [SetBackground]}, 
    { path: 'home', component: HomePageComponent}, 
    { path: 'currentWork', component: CurrentWorkComponent}, 
    { path: 'progress', component: InProgressComponent},
    { path: 'currentWork/workOne/:id', component: WorkOneComponent},
    { path: 'aboutMe', component: AboutMeComponent},
    { path: 'contactMe', component: ContactMeComponent},
    { path: 'test', component: Test},
    { path: '', component: HomePageComponent}
];
