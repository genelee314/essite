import { Injectable, OnInit } from '@angular/core';
import { ICurrentWorkData } from '../data/ICurrentWorkData';
import { ThumbnailData } from '../services/thumbnailData.service';
import { IThumbnail } from '../data/IThumbnailData';

@Injectable()
export class CurrentWorkData implements OnInit{
   /* getThumbnailData(): Observable<IThumbnail[]> {
        const subject = new Subject<IThumbnail[]>();
        setTimeout (() => {subject.next(CURRENTWORKDATA); subject.complete(); }, 100);
        return subject;
    }  */
    private tn: ThumbnailData;
    private str: string;
    
    constructor (private thumbnails: ThumbnailData) {
        this.str = 'new';
        this.tn = thumbnails;
        this.str = 'old';
    }

    ngOnInit() {
    }

    getCurrentWorkData(): ICurrentWorkData[] {
        this.thumbnails.getThumbnails();
        for ( let i = 0; i < CURRENTWORKDATA.length; i++) {
            CURRENTWORKDATA[i].pictureThumbnail = this.thumbnails.getThumbnail(i);
         }
        return CURRENTWORKDATA;
    }

    getCurrentSingleWork(id: number): ICurrentWorkData {
        return CURRENTWORKDATA[id];
    }

    getLength(): number {
        return CURRENTWORKDATA.length;
    }
}

const CURRENTWORKDATA: ICurrentWorkData[] = [
    {
        projectNumber: 0,
        backGroundUrl: '/assets/SAM_2000.jpg',
        pictureUrls : ['/assets/SAM_2000.jpg', '/assets/background.jpg', '/assets/SAM_2000_.JPG', '/assets/SAM_67.JPG'],
        title: 'Day Break',
        description: 'This work started with a lot of ideas, mostly about desire and affirmation. As I worked on the figures, and brought them together, they \
        started leading me. After some struggle, I let figures guide me.  They emerged with their own statement, and I \
        must confess, it is better than mine had been.',
        text: 'I wanted to do something to suggest both the body and the spirit, and the joy when the body \
        joins with the spirit. <br/><br/> \
        I envisioned two figures - one bound the ground, one floating above. they would be interacting, but still appearing seperate. \
        I tried to devise a situation where one figure appears weightless and floating, as a spirit might. \
        The other figure is being lifted from the ground, as if the body is being lifted by the spirit. <br/><br/> \
        The only contact between the figures is in the touch of a fingertip. Yet in this small touch, body and spirit may be joined. '
    },
    {
        projectNumber: 1,
        backGroundUrl: '/assets/SAM_67.JPG',
        pictureUrls : ['/assets/SAM_67.JPG', '/assets/SAM_0763.JPG', '/assets/SAM_67.JPG'],
        title: 'Touch',
        description: 'I wanted to try something intimate, perhaps the special moment a person touches \
        someone beyond themselves, and in that moment glimpses a spirit that reminds us how wondrous life can be. ',
        
        text: 'I wanted to try a arrangement that was both intimate and quiet, but also makes a strong emotional statement - \
        perhaps about grace and acceptance. One figure is reaching out,  floating. The other is tentative,  uncertain how to respond.<br/><br/>\
        As I completed the bodies, It seemed that the hands told the story. The floating figure is\
        reaching out - both hands extended to the other figure.  One hand is open, as a priest might offering a blessing. The other is just touching \
        , in unconditional acceptance. \
        The grounded figure is conflicted. One hand is raised in defense of the intruision, the other is passively accepting what is offerred. <br/><br/>\
        Perhaps in this special moment when a person touches something outside themselves, one  glimpses some deeper meaning in life. \
        It may be a feeling of fulfilment, of acceptance, of love, of grace, or of peace.'
    },
    {
        projectNumber: 2,
        backGroundUrl: '/assets/SAM_2135_.JPG',
        pictureUrls : ['/assets/SAM_0763.JPG', '/assets/SAM_0763.JPG', '/assets/SAM_67.JPG'],
        title: 'Two Choices',
        description: 'This is an attempt to explore the emotional relationships from closely intertwined bodies. \
        I started with vague idea of the feeling that I was searching for when I began contruction, but as I worked on the figures, they took on a \
        completely different feeling. At first I was uncomfortable with the direction, but as I followed their direction, I became \
        very excited about the results.',         
        text: 'Watching figure skating, I was taken by the stories created \
        as the dancers skated. I liked the emotions that \
        were evoked by the many variations of the movements. <br/><br/>I had a vague idea of the feeling I was \
        searching for in their construction, but as I worked on the figures, I was not comfortable with relationship that \
        I had planned. I spent some time rearranging the figure, trying new positions, trying to find the spirit in the figures. <br/><br/>\
        I finally let the bodies lead the composition, and I followed. At first I was uncomfortable with what was unfolding, \
        but as I followed them, I became very excited about the results. The result is slightly ambiguous, but is also one \
        of the most interesting pieces that I have worked on!'
    }
];



