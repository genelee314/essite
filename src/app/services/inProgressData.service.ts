import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable } from 'rxjs/RX';
import { IInProgress } from '../data/IInProgressData';

@Injectable()
export class InProgress {
    getInProgressData(): Observable<IInProgress[]> {
        const subject = new Subject<IInProgress[]>();
        setTimeout (() => {subject.next(INPROGRESSDATA); subject.complete(); }, 100);
        return subject;
    }

    getInProgressData_(): IInProgress[] {
        return  INPROGRESSDATA;
    }

    getInProgressSingleWork(id: number): IInProgress {
        return INPROGRESSDATA[id];
    }

    getLength(): number {
        return INPROGRESSDATA.length;
    }
}

const INPROGRESSDATA: IInProgress[] = [
    {
        postNumber: 1,
        title: 'Creating a head',
        description: 'Creating the head has two problems - there are a number of small details that must be assembled, \
        and the head must be symmetrical. Flat metal is not good for this. It difficult to render small details without \
        the results looking clunky, and it very difficult to create matching curved shapes. For the smaller pieces like the \
        eyes and mouth, it is easier to create them separately, and solder the completed assembly into place',
        pictureUrls:  ['/assets/workProgress/SAM_1982.JPG', '/assets/workProgress/SAM_1984.JPG', '/assets/workProgress/SAM_1986.JPG', '/assets/workProgress/SAM_1959.JPG']
    },
    {
        postNumber: 2,
        title: 'Looking for the drama in the movement',
        description: 'This is where things do get fun. I have the basic figures blocked out. Now I need to adjust the positions so that they will \
        fit together properly, and create the right mood in space. ',
        pictureUrls:  ['/assets/workProgress/SAM_1882.JPG', '/assets/workProgress/SAM_1884.JPG', '/assets/workProgress/SAM_1558.JPG']
    },
    {
        postNumber: 3,
        title: 'We need to start somewhere',
        description: 'These figures all start with a firm support. Standard iron and copper water piping is good for this. The iron piping is strong \
        and rigid, but joint angles are limited. Copper provides good support for the shoulders and arms, and can the joints can be a finely tuned to the \
        required position. ',
        pictureUrls:  ['/assets/workProgress/SAM_0643.JPG', '/assets/workProgress/SAM_0644.JPG', '/assets/workProgress/SAM_0648.JPG']
    },
    {
        postNumber: 4,
        title: 'Man was created from the clay of the earth',
        description: 'Clay figures are a great way to try out an idea to see if it is promising. I created a metal armature with flexable \
        joints for the clay figures.  I cover the armature and quickly have a basic figure. From there I can move, adjust, and reset \
        the figure until I get something worth pursuing.',
        pictureUrls:  ['/assets/workProgress/SAM_1745.JPG', '/assets/workProgress/SAM_19444.JPG', '/assets/workProgress/SAM_1950.JPG']
    }
];
