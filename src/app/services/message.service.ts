import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { ICurrentWorkData } from '../data/ICurrentWorkData';

@Injectable()
export class MessageService {
    private subject = new Subject();
    
    sendMessage(message: any) {
        this.subject.next({ text: message });
    }

    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}