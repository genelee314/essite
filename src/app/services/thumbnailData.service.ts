import { Injectable, EventEmitter } from '@angular/core';
import { IThumbnail } from '../data/IThumbnailData';

@Injectable()
export class ThumbnailData {
    getThumbnails(): IThumbnail[] {
        return THUMBNAILS;
    }

    getThumbnail(id: number): IThumbnail {
        return THUMBNAILS[id];
    }

    getThumbnailClass(): string {
        return 'aboutButton';
    }

    getPictureClass(): string {
        return 'aboutPicture';
    }
}
const THUMBNAILS: IThumbnail[] = [
    {
        thumbnail: './assets/SAM_67.JPG',
        picture: './assets/SAM_67.JPG',
        description: 'This is a description.'
    },
    {
        thumbnail: './assets/SAM_0763.JPG',
        picture: './assets/SAM_0763.JPG',
        description: 'This is a description.'
    },
    {
        thumbnail: './assets/SAM_2000.jpg',
        picture: './assets/SAM_2000.jpg',
        description: 'This is a description.'
    }
];
